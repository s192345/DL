import pandas as pd 
import numpy as np
import datetime
from sklearn import preprocessing
def data_load():
    data_raw = pd.read_csv("Our_data.csv") 

    # Preview the first 5 lines of the loaded data 
    timestamp_df=data_raw['Reservationstidspunkt']
    data_raw=data_raw[['Latitude (Start)','Longitude (Start)']]
    #timestamp_df.to_datetime()
    datetime=pd.to_datetime(timestamp_df)
    days_series=datetime.dt.dayofweek
    days_series=days_series.replace({1:0,2:0,3:0,4:0,5:1,6:1})
    data_raw['Day']=days_series
    data_raw.columns=['Latitude','Longitude','Day']
    data_weekday=data_raw.loc[data_raw['Day']==0]
    data_weekend=data_raw.loc[data_raw['Day']==1]

    data_weekday=data_weekday[['Longitude','Latitude']]
    data_weekend=data_weekend[['Longitude','Latitude']]

    data_weekday=data_weekday[data_weekday['Latitude']!='-']
    data_weekday=data_weekday[data_weekday['Longitude']!='-'].astype(float).to_numpy()

    data_weekend=data_weekend[data_weekend['Latitude']!='-']
    data_weekend=data_weekend[data_weekend['Longitude']!='-'].astype(float).to_numpy()
    
    data_weekend=preprocessing.scale(data_weekend)
    data_weekday=preprocessing.scale(data_weekday)
    return data_weekday,data_weekend
